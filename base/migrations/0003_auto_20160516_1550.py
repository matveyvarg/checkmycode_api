# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-16 15:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20160516_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comment_author', to='base.UserProfile'),
        ),
        migrations.AlterField(
            model_name='entry',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='entry', to='base.UserProfile'),
        ),
    ]
