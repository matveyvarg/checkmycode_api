from django.contrib.auth import authenticate, login, logout

def user_login(request,username,password):
    #authenticate user by standart system

    user_auth = authenticate(username=username,password=password)
    if user_auth:
        if user_auth.is_active:
            login(request,user_auth)
            return True
        else:
            return False
    else:
        return False
