from django.shortcuts import render
from django.http import Http404
from django.contrib.auth.models import User

from base.models import *
from base.serializers import *
from base.user_auth import *

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics


# Create your views here.
class EntryList(generics.ListCreateAPIView):
    #GET request to list all entrys
    queryset = Entry.objects.all()
    serializer_class = EntrySerializer
#User information
class UserView(generics.RetrieveAPIView):
    queryset  = User.objects.all()
    serializer_class = UserSerializer

class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
