from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

#Langs array to filter list
langs = (
    ('java','Java'),
    ('php','PHP'),
    ('python','Python'),
    ('ruby','Ruby'),
    ('javascript','JavaScript'),
)

class UserProfile(models.Model):
    user = models.OneToOneField(User,related_name='user_profile')
    level = models.IntegerField(default=0)
    nextlvl_exp = models.IntegerField(default=0)
    currlvl_exp = models.IntegerField(default=0)
    code_rating = models.DecimalField(default=0,decimal_places=2,max_digits=10)
    def __unicode__(self):
        return self.user.username

class Entry(models.Model):
    source = models.TextField(max_length=2000)
    description = models.TextField(max_length=300)
    author = models.ForeignKey(UserProfile,related_name='entry',null=True,blank=True)
    lang = models.CharField(choices = langs,default='python',max_length=20)
    highlight = models.CharField(default='',max_length=2000)


class Comment(models.Model):
    author = models.ForeignKey(UserProfile,related_name='comment_author',null=True)
    entry = models.ForeignKey(Entry,related_name='comment_entry')
    text = models.CharField(max_length=500)
    first_line = models.IntegerField(default=0)
    last_line = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    def __unicode__(self):
        return self.author.user.username

class Mistake(models.Model):
    author = models.ForeignKey(UserProfile,related_name='mistake_author')
    entry = models.ForeignKey(Entry,related_name='mistake_entry')
    first_line = models.IntegerField(default=0)
    last_line = models.IntegerField(default=0)
    def __unicode__(self):
        return self.author.user.username
