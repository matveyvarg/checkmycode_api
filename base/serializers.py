from rest_framework import serializers
from base.models import *
#Import pygments for highlight_code
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter
from user_auth import user_login
#Function for highlighting code
def highlight_code(code,lang):
    lexer = get_lexer_by_name(lang, stripall=True)
    formatter = HtmlFormatter(linenos=True, cssclass="codehilite",style="friendly")
    result = highlight(code, lexer, formatter)
    return result
#serializer for Entry
class EntrySerializer(serializers.ModelSerializer):
    #Create new entry
    def create(self, validated_data):
        #Sourc and lang for highlight
        source = validated_data['source']
        lang = validated_data['lang']

        #highlight code return
        validated_data['highlight'] = highlight_code(source,lang)
        return Entry.objects.create(**validated_data)

    class Meta:
        model = Entry
        fields = ['source','description','lang','highlight','pk','author']

class UserSerializer(serializers.ModelSerializer):
    level = serializers.IntegerField(source="user_profile.level",read_only=True)
    def validate_email(self,email):
        if email == '':
            raise serializers.ValidationError("Email field is required")
        return email
    class Meta:
        model = User
        fields = ['id', 'username', 'email','password','level']

        extra_kwargs = {'password': {'write_only': True}}


    def create(self, validated_data):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
        user = User(email=validated_data['email'], username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        request  =  self.context['request']
        if not user_login(request,validated_data['username'],validated_data['password']):
            raise serializer.ValidationError("Login failed")
        return user

class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email','password']
        extra_kwargs = {'password': {'write_only': True}}
